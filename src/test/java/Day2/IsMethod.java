package Day2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class IsMethod {

	public static void main(String [] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\USER\\eclipse-workspace\\SeleniumPrograms\\Resources\\chromedriver.exe");
		
		WebDriver driver ;					
		 driver =new  ChromeDriver();				 
		 driver.get("https://paytm.com/");
		 driver.findElement(By.xpath("//span[text()='Mobile Prepaid']")).click();
		 Thread.sleep(5000);
		 boolean f = driver.findElement(By.xpath("//div[text()='Mobile Recharge or Bill Payment']")).isDisplayed();
		 System.out.println(f);
		 boolean flag = driver.findElement(By.xpath("//label[text()='Postpaid']")).isSelected();
			System.out.println(flag);
	    driver.findElement(By.xpath("//label[text()='Postpaid']")).click();
		 		
	}
	
}
