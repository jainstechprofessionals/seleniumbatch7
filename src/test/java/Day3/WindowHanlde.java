package Day3;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHanlde {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\USER\\eclipse-workspace\\SeleniumPrograms\\Resources\\chromedriver.exe");

		WebDriver driver;
		driver = new ChromeDriver();

		driver.get("https://www.seleniumeasy.com/test/window-popup-modal-demo.html");
		driver.findElement(By.xpath("//a[contains(text(),'Follow All')]")).click();

		String cuWinId = driver.getWindowHandle();
		System.out.println(cuWinId);

		Set<String> winHandle = driver.getWindowHandles();

		Iterator<String> itr = winHandle.iterator();

		while (itr.hasNext()) {
			String childWind = itr.next();
			System.out.println(childWind);
			String str =driver.switchTo().window(childWind).getCurrentUrl();

			if(str.contains("facebook")) {
				break;
			}
			
		}
		
		
		String curUrl= driver.getCurrentUrl();
		System.out.println(curUrl);
		driver.switchTo().window(cuWinId);

	}
}
