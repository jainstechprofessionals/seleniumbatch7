package Day3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FrameHandle {

	public static void main(String[] args) {
		

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\USER\\eclipse-workspace\\SeleniumPrograms\\Resources\\chromedriver.exe");
		
		WebDriver driver ;					
		 driver =new  ChromeDriver();	
		 
		 driver.get("https://jqueryui.com/droppable/");
		 
		 WebElement frame1 = driver.findElement(By.xpath("//iframe[@class='demo-frame']"));
		
		 driver.switchTo().frame(frame1);
		 
		 
		 WebElement we =driver.findElement(By.xpath("//p[text()='Drag me to my target']"));
		boolean flag = we.isDisplayed();
		System.out.println(flag);
		
		
				
	}

}
