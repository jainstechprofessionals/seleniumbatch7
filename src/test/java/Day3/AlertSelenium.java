package Day3;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertSelenium {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\USER\\eclipse-workspace\\SeleniumPrograms\\Resources\\chromedriver.exe");
		
		WebDriver driver ;					
		 driver =new  ChromeDriver();
		 
		driver.get("https://www.seleniumeasy.com/test/javascript-alert-box-demo.html");
		
		driver.findElement(By.xpath("(//button[text()='Click me!'])[1]")).click();
		
		Thread.sleep(3000);
		
		Alert ale = driver.switchTo().alert();
		
		String str = ale.getText();
		System.out.println(str);
			
		ale.accept();
		
		Thread.sleep(3000);
		driver.findElement(By.xpath("(//button[text()='Click me!'])[2]")).click();	
		Thread.sleep(3000);
		ale = driver.switchTo().alert();
		str = ale.getText();
		System.out.println(str);
		ale.dismiss();
		//p[text()='You pressed Cancel!']
		boolean flag =driver.findElement(By.xpath("//p[text()='You pressed Cancel!']")).isDisplayed();
		System.out.println(flag);
		
		

		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()='Click for Prompt Box']")).click();	
		Thread.sleep(3000);
		ale = driver.switchTo().alert();
		str = ale.getText();
		System.out.println(str);
		ale.sendKeys("rounak");
		ale.accept();
		String val=driver.findElement(By.xpath("//p[contains(text(),'You have entered')]")).getText();
		
		if(val.contains("rounak")) {
			System.out.println("sakjdfldsajfl;sad");
		}
		
		
	}

}
