package Day6;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class FindELements {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\USER\\eclipse-workspace\\SeleniumPrograms\\Resources\\chromedriver.exe");
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.flipkart.com/");
		
		
		Actions act = new Actions(driver);
		By loc = By.xpath("//span[text()='Electronics']");
		act.moveToElement(driver.findElement(loc)).perform();
		List <WebElement> we =driver.findElements(By.xpath("//span[text()='Electronics']/following-sibling::ul//li/a"));
		System.out.println(we.size());
		
		for(int i=0;i<we.size();i++) {
			String str =we.get(i).getAttribute("title");
			System.out.println(str);
		}
		

	}

}
