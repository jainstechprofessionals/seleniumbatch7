package Day6;




import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Screenshot {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\USER\\eclipse-workspace\\SeleniumPrograms\\Resources\\chromedriver.exe");
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.flipkart.com/");
		
		
		TakesScreenshot scrsh= (TakesScreenshot)driver;
		
		File srcFile = scrsh.getScreenshotAs(OutputType.FILE);
        File target = new File("C:\\Users\\USER\\Desktop\\New folder\\a.jpeg");
        FileUtils.copyFile(srcFile,target);
        
        
	}

}
