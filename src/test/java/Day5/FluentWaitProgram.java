package Day5;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class FluentWaitProgram {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\USER\\eclipse-workspace\\SeleniumPrograms\\Resources\\chromedriver.exe");
		WebDriver driver;
		driver = new ChromeDriver();

		Wait<WebDriver> wat = new FluentWait<WebDriver>(driver).withTimeout(30, TimeUnit.SECONDS).pollingEvery(5,
				TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

		driver.get("https://demo.testfire.net");
		driver.manage().window().maximize();
		
		  driver.findElement(By.xpath("//font[text()='Sign In']")).click();
		  driver.findElement(By.id("uid")).sendKeys("jsmith");		  
		  
		  driver.findElement(By.id("passw")).sendKeys("Demo1234");
		  wat.until(ExpectedConditions.elementToBeClickable(By.name("btnSubmit")));
		  driver.findElement(By.name("btnSubmit")).click();
		  driver.navigate().refresh(); driver.navigate().back();
		  driver.navigate().forward(); driver.navigate().to("");
		 

	}

}
