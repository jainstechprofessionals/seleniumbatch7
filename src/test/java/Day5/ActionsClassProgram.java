package Day5;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ActionsClassProgram {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\USER\\eclipse-workspace\\SeleniumPrograms\\Resources\\chromedriver.exe");
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://jqueryui.com/droppable/");
		
		driver.findElement(By.xpath("//a[text()='Resizable']")).click();
		Actions act = new Actions(driver);
		
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@class='demo-frame']")));
		
		
		//WebElement src = driver.findElement(By.xpath("//p[text()='Drag me to my target']"));
		//WebElement target = driver.findElement(By.xpath("//p[text()='Drop here']"));
		
		WebElement s =  driver.findElement(By.xpath("//div[contains(@class,'ui-resizable-se')]"));
		
//		act.dragAndDrop(src, target).build().perform();
//		act.release();
	  
		act.sendKeys(Keys.PAGE_DOWN).dragAndDropBy(s, 105, 120).build().perform();
		
	}

}
