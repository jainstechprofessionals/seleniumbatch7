package Day5;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitActivity {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\USER\\eclipse-workspace\\SeleniumPrograms\\Resources\\chromedriver.exe");		
		WebDriver driver ;					
		 driver =new  ChromeDriver();	
		 
		 driver.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
		 WebDriverWait wait= new WebDriverWait(driver, 3000);
		 
		 
		 driver.get("https://demo.testfire.net");
		 driver.manage().window().maximize();
		 
//		 driver.navigate().to("https://demo.testfire.net");		 
		 driver.findElement(By.xpath("//font[text()='Sign In']")).click();
		 
		 driver.findElement(By.id("uid")).sendKeys("jsmith");
		 driver.findElement(By.id("passw")).sendKeys("Demo1234");
		 wait.until(ExpectedConditions.elementToBeClickable(By.name("btnSubmit")));
		 
		 
		 driver.findElement(By.name("btnSubmit")).click();
		 
		 
		 
		 
		 
		 

	}

}
