package Day1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Day1SeleniumP1 {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\USER\\eclipse-workspace\\SeleniumPrograms\\Resources\\chromedriver.exe");		
		WebDriver driver ;					
		 driver =new  ChromeDriver();	
		 
		 driver.get("https://demo.testfire.net");
		 driver.manage().window().maximize();
		 
//		 driver.navigate().to("https://demo.testfire.net");		 
		 driver.findElement(By.xpath("//font[text()='Sign In']")).click();
		 
		 driver.findElement(By.id("uid")).sendKeys("jsmith");
		 driver.findElement(By.id("passw")).sendKeys("Demo1234");
		 driver.findElement(By.name("btnSubmit")).click();
		 
		 String str = driver.findElement(By.xpath("//h1[contains(text(),'Hello')]")).getText();
		 
		 if(str.contains("John")) {
			 System.out.println("User is able to login");
		 }else {
			 System.out.println("Not able to Login");
		 }	 
		 
		 WebElement we = driver.findElement(By.id("listAccounts"));
		 Select sel = new Select(we);
		 
		
		 
		sel.selectByValue("800003");
		List<WebElement>lst = sel.getOptions();
		for(int i=0;i<lst.size();i++) {
			System.out.println(lst.get(i).getText());
		}
		
		List<WebElement> ste =sel.getAllSelectedOptions();
		 
		for(int i=0;i<ste.size();i++) {
			System.out.println(ste.get(i).getText());
		}
		 
		Thread.sleep(5000);		
		
	}

}
