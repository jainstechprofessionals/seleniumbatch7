package Day1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Program2 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\USER\\eclipse-workspace\\SeleniumPrograms\\Resources\\chromedriver.exe");		
		WebDriver driver ;					
		 driver =new  ChromeDriver();	
		 driver.get("https://demoqa.com/automation-practice-form/");
		 
		 driver.findElement(By.id("firstName")).sendKeys("Rounak");
		 driver.findElement(By.id("lastName")).sendKeys("Jain"); 
		 driver.findElement(By.id("userEmail")).sendKeys("demo@gmail.com"); 
		// driver.findElement(By.id("gender-radio-1")).click();
		 driver.findElement((By.xpath("//label[text()='Sports']"))).click();
		driver.findElement(By.id("uploadPicture")).sendKeys("C:\\Users\\USER\\Desktop\\New folder\\New Text Document.txt");	 
		 driver.findElement(By.id("submit")).click();
		 boolean flag =driver.findElement(By.xpath("//div[text()='Thanks for submitting the form']")).isDisplayed();
		 if(flag==true) {
			 System.out.println("User registered Successfully");
		 }else {
			 
			 System.out.println("User did not registered");
		 }
		 
		 
		 
	}

}
